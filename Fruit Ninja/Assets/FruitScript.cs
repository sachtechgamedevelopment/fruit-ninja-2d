﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitScript : MonoBehaviour
{

    Rigidbody2D rb;
    float startForce =14f;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(transform.up*startForce,ForceMode2D.Impulse);
    }
    public GameObject slice;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag.Equals("blade")) {
            Vector3 direction = (other.transform.position - transform.position).normalized;
            Quaternion rotation=Quaternion.LookRotation(direction);
            GameObject sliceF = Instantiate(slice,transform.position,rotation);
            Destroy(gameObject);
          }
    }
    private void OnBecameInvisible()
    {
        DestroyImmediate(gameObject);
    }
}
