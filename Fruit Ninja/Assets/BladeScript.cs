﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeScript : MonoBehaviour
{
    Rigidbody2D rb;
    public GameObject trail;
    bool isCutting = false;
    public float minCuttingVilocity ;
    GameObject currentTrail;
    CircleCollider2D circleCollider;
    Vector2 prevoius;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        circleCollider = GetComponent<CircleCollider2D>();
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0)) {
            startCutting();
        }
        else if (Input.GetMouseButtonUp(0)) {
            stopCutting();
        }

        if (isCutting) {
            updateCut();
        }
        
    }

    private void updateCut()
    {
        Vector2 newPosition= Camera.main.ScreenToWorldPoint(Input.mousePosition);
        rb.position = newPosition;
        float vilocity = (newPosition - prevoius).magnitude/Time.deltaTime;
        if (vilocity > minCuttingVilocity) {
            circleCollider.enabled = true;
        }
        else {
            circleCollider.enabled = false;
        }
        prevoius = newPosition;
    }

    private void startCutting()
    {
        isCutting = true;
        currentTrail=Instantiate(trail,transform);
        circleCollider.enabled = false;
        prevoius = Camera.main.ScreenToWorldPoint(Input.acceleration);
}

    private void stopCutting()
    {
        isCutting = false;
        currentTrail.transform.SetParent(null);
        circleCollider.enabled = false;
        Destroy(currentTrail, 1f);
    }
   
   }
