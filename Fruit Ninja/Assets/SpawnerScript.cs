﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    public GameObject fruit;
    public Transform[] spawnPoints;

    public float minDelay = .1f;
    public float maxDelay = .4f;
    void Start()
    {
        StartCoroutine(SpawnFruits());
    }
    IEnumerator SpawnFruits(){
        while (true) {
            float delay = Random.Range(minDelay, maxDelay);
            yield return new WaitForSeconds(delay);
            int spawnIndex = Random.Range(0, spawnPoints.Length);
            Transform fTransform = spawnPoints[spawnIndex];
            GameObject fObj=Instantiate(fruit,fTransform.position,fTransform.rotation);
            Destroy(fObj, 2.5f);
         }
    }
}
